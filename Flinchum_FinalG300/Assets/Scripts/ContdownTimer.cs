﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ContdownTimer : MonoBehaviour {

	public float timeLeft = 300.0f;

		public Text text;

	private void Update()
	{
		timeLeft -= Time.deltaTime;
		text.text = "Time Left:" + Mathf.Round (timeLeft);
		if (timeLeft < 0) {
			GameOver ();
		}
	}

	private void GameOver()
	{
		SceneManager.LoadScene(0);
	}
}
