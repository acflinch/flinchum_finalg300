﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent (typeof (BoxCollider))]
public class BoxGizmos : MonoBehaviour {

	public Color gizmoColor;

	void OnDrawGizmos() {
		Gizmos.color = gizmoColor;
		Gizmos.DrawCube (transform.position + GetComponent<BoxCollider> ().center, GetComponent<BoxCollider> ().size);
	}
}
