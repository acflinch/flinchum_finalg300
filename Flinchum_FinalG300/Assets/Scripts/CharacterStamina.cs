﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStamina : MonoBehaviour {

	public static float currentStamina;
	public float maxStamina;

	public bool isRegenHealth;
	public bool run;

	public Slider staminaBar;

	void Start () {
		maxStamina = 45f;
		currentStamina = maxStamina;

		staminaBar.value = CalculateStamina ();
	}
			
	void Update () {

		if(!isRegenHealth && !run) {
			StartCoroutine(RegainHealthOverTime());
		}
		if (run) {
			StartCoroutine(RunOverTime());
		}
		if (Input.GetKeyDown (KeyCode.W)) {
			isRegenHealth = true;
			UseStamina (15);
		}
		else if (Input.GetKeyUp (KeyCode.W)) {
			isRegenHealth = false;
		}
		if (Input.GetKeyDown (KeyCode.LeftShift)) {
			isRegenHealth =true;
			run = true;
		}
		else if(Input.GetKeyUp (KeyCode.LeftShift)) {
			isRegenHealth = false;
			run = false;
		}
	}

	void UseStamina(float staminaValue) {
		currentStamina -= staminaValue;
		staminaBar.value = CalculateStamina ();

	}

	void reStamina(float staminaValue) {
		currentStamina += staminaValue;
		staminaBar.value = CalculateStamina ();

	}

	private IEnumerator RegainHealthOverTime() {
		isRegenHealth = true;
		while (currentStamina < maxStamina) {
			reStamina (1);
			yield return new WaitForSeconds (.5f);
		}
		isRegenHealth = false;
	}

	private IEnumerator RunOverTime() {
		while (run) {
			UseStamina (.05f);
			yield return new WaitForSeconds (50);
		}
	}

	float CalculateStamina() {
		return currentStamina / maxStamina;
	}
}
